﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;

namespace SQLHelper
{
    /// <summary>
    /// 数据库操作的单例类：SQLHelperSingleton
    /// 这个类被拆成两个文件：SQLHelperSingleton.cs、SQLHelperAdapter.cs和SQLHelperEntity.cs
    /// 第一个主要写该类的一些基本属性及其配置方法，第二个主要写可供外部调用的SQL操作方法，最后一个主要写具体的SQL操作方法
    /// </summary>
    public partial class SQLHelperSingleton
    {
        /// <summary>
        /// 尝试执行查询语句，如果成功，将执行success代理方法，若执行中出现错误，将执行fail代理
        /// </summary>
        /// <param name="sqlStr"></param>
        /// <param name="success"></param>
        /// <param name="fail"></param>
        static public void TryQueryByDataSet (string sqlStr, FinishWithDataSet success, ExecuteFail fail)
        {
            try
            {
                SQLHelperSingleton.ShareSingletion().QueryByDataSet(sqlStr, success);
            } catch ( SqlException ex )
            {
                if ( fail != null )
                {
                    fail(ex.Message);
                }
            }
        }

        /// <summary>
        /// 尝试执行查询语句，如果成功，将执行success代理方法，若执行中出现错误，将执行fail代理
        /// </summary>
        /// <param name="sqlStr"></param>
        /// <param name="success"></param>
        /// <param name="fail"></param>
        static public void TryQueryByDataReader (string sqlStr, FinishWithDataReader success, ExecuteFail fail)
        {
            try
            {
                SQLHelperSingleton.ShareSingletion().QueryByDataReader(sqlStr, success);
            } catch ( SqlException ex )
            {
                if ( fail != null )
                {
                    fail(ex.Message);
                }
            }
        }

        /// <summary>
        /// 执行SqlCommand的ExecuteScalar方法
        /// </summary>
        /// <param name="sqlStr"></param>
        /// <param name="success"></param>
        /// <param name="fail"></param>
        static public void TryExecuteScalar (string sqlStr, FinishWithScalar success, ExecuteFail fail)
        {
            try
            {
                SQLHelperSingleton.ShareSingletion().QueryScalar(sqlStr, success);
            } catch ( SqlException ex )
            {
                if ( fail != null )
                {
                    fail(ex.Message);
                }
            }
        } 

        /// <summary>
        /// 尝试执行增删改操作
        /// </summary>
        /// <param name="sqlStr"></param>
        /// <param name="fail"></param>
        static public void TryExecuteNonQuery (string sqlStr, ExecuteFail fail)
        {
            try
            {
                SQLHelperSingleton.ShareSingletion().ExecuteNonQuery(sqlStr);
            } catch ( SqlException ex )
            {
                if ( fail != null )
                {
                    fail(ex.Message);
                }
            }
        }
    }
}
