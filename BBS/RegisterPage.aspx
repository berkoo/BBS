﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="RegisterPage.aspx.cs" Inherits="BBS.RegisterPage" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style>
        #form{
            width:60%;
            margin:0px auto;
        }
        .key{
            width:20%;
        }
        .content{
            width:80%;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Content1" runat="server">
    <div style="margin:0px auto; width:100%;">
        <table id="form">
            <tr>
                <td class="key">昵称：</td>
                <td class="content">
                    <asp:TextBox ID="txtPickName" runat="server"></asp:TextBox>
                    <asp:RequiredFieldValidator runat="server" ControlToValidate="txtPickName" ErrorMessage="*" ForeColor="Red"></asp:RequiredFieldValidator>
                    <asp:Label ID="lblRepeat" runat="server" Text="该昵称已被使用" ForeColor="Red" Visible="false"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="key">密码：</td>
                <td class="content">
                    <asp:TextBox ID="txtPwd" runat="server" TextMode="Password"></asp:TextBox>
                    <asp:RequiredFieldValidator runat="server" ControlToValidate="txtPwd" ErrorMessage="*" ForeColor="Red"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td class="key">确认密码：</td>
                <td class="content">
                    <asp:TextBox ID="txtEnsurePwd" runat="server" TextMode="Password"></asp:TextBox>
                    <asp:RequiredFieldValidator runat="server" ControlToValidate="txtEnsurePwd" ErrorMessage="*" ForeColor="Red"></asp:RequiredFieldValidator>
                    <asp:CompareValidator runat="server" ControlToCompare="txtPwd" ControlToValidate="txtEnsurePwd" ErrorMessage="两次密码输入不一致!" ForeColor="Red"></asp:CompareValidator>
                </td>
            </tr>
            <tr>
                <td><asp:Button ID="btnRegister" runat="server" OnClick="btnRegister_Click" Text="注册" /></td>
            </tr>
        </table>
    </div>
</asp:Content>
