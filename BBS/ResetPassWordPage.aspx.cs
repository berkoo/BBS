﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SQLHelper;

namespace BBS
{
    public partial class ResetPassWordPage : BasePage
    {
        private bool isTrueOfUsingPwd;

        protected void Page_Load (object sender, EventArgs e)
        {
            if ( !IsPostBack )
            {
                this.isTrueOfUsingPwd = false;
            }
        }

        protected void btnSubmit_Click (object sender, EventArgs e)
        {
            User user = (User)Session["user"];
            if ( user == null )
            {
                this.Alert("您未登陆或登陆已过期，请重新登陆！");
                this.RedirectTo("Login.aspx");
            }

            //检查旧密码是否输入正确
            string queryPwdStr = string.Format("SELECT PWD FROM ACCOUNT WHERE PICKNAME='{0}';", user.PickName);
            SQLHelperSingleton.TryExecuteScalar(queryPwdStr, this.ComparePwd, null);

            if ( this.isTrueOfUsingPwd == false )
            {
                this.Alert("旧密码输入错误！");
                return;
            }

            //修改密码
            string updatePwdStr = string.Format("UPDATE ACCOUNT SET PWD='{0}' WHERE ACCOUNT_ID='{1}';", this.txtNewPwd.Text, user.UserId);
            SQLHelperSingleton.TryExecuteNonQuery(updatePwdStr, null);

            this.Alert("密码修改成功！");
        }

        /// <summary>
        /// 检查旧密码是否输入正确
        /// </summary>
        /// <param name="pwd"></param>
        private void ComparePwd (object pwd)
        {
            if ( pwd.ToString() != this.txtUsingPwd.Text )
            {
                this.isTrueOfUsingPwd = false;
            } else
            {
                this.isTrueOfUsingPwd = true;
            }
        }
    }
}