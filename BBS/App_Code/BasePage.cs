﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using MessageBoxHelper;

namespace BBS
{
    /// <summary>
    /// 该类将作为所有页面隐藏类的基类
    /// 该类定义了一些所有页面通用的方法，方便一些方法的调用
    /// </summary>
    public class BasePage : Page
    {
        /// <summary>
        /// 执行js代码
        /// 该方法是为了取代Response.Write，因为后者有某些局限
        /// </summary>
        /// <param name="jsCode"></param>
        protected void ExecuteJS (string jsCode)
        {
            if ( string.IsNullOrEmpty(jsCode) )
            {
                return;
            }

            this.ClientScript.RegisterStartupScript(this.GetType(), "", JSHelper.GetScriptStringWithJSCodeString(jsCode));
        }

        /// <summary>
        /// 执行弹框
        /// </summary>
        /// <param name="alertContent"></param>
        protected void Alert (string alertContent)
        {
            string content = string.Format("alert('{0}');", alertContent);
            this.ExecuteJS(content);
        }

        /// <summary>
        /// 重定向到其他页面
        /// 注：这里用的不是Response
        /// </summary>
        /// <param name="pagePath"></param>
        protected void RedirectTo (string pagePath)
        {
            this.ExecuteJS(string.Format("window.location.href='{0}'", pagePath));
        }
    }
}