﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BBS
{
    public class ReplyModel
    {
        //评论所属文章id
        private int themeId;
        public int ThemeId
        {
            get
            {
                return this.themeId;
            }
        }

        //该评论的id
        private int replyId;
        public int ReplyId
        {
            get
            {
                return replyId;
            }
        }

        //被评论的评论的id，可空
        private int targetReplyId;
        public int TargetReplyId
        {
            get
            {
                return targetReplyId;
            }
        }

        //评论人的id
        private int replyAccountId;
        public int ReplyAccountId
        {
            get
            {
                return replyAccountId;
            }
        }

        //被评论的用户的id
        private int targetAccountId;
        public int TargetAccountId
        {
            get
            {
                return targetAccountId;
            }
        }

        //评论人的昵称
        private string replyAccountPickName;
        public string ReplyAccountPickName
        {
            get
            {
                return replyAccountPickName;
            }
        }

        //被评论的用户的昵称
        private string targetAccountPickName;
        public string TargetAccountPickName
        {
            get
            {
                return targetAccountPickName;
            }
        }

        //该评论的日期
        private string replyDate;
        public string ReplyDate
        {
            get
            {
                return replyDate;
            }
        }

        //该评论的内容
        private string replyContent;
        public string ReplyContent
        {
            get
            {
                return replyContent;
            }
        }

        public ReplyModel (int TI, int RI, int TRI, int RAI,
            int TAI, string RAP, string TAP, string RD, string RC)
        {
            this.themeId = TI;
            this.replyId = RI;
            this.targetReplyId = TRI;
            this.replyAccountId = RAI;
            this.targetAccountId = TAI;
            this.replyAccountPickName = RAP;
            this.targetAccountPickName = TAP;
            this.replyDate = RD;
            this.replyContent = RC;
        }
    }
}