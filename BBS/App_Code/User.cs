﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BBS
{
    public class User
    {
        protected string pickName;
        public string PickName
        {
            get
            {
                return pickName;
            }
        }
        protected string userId;
        public string UserId
        {
            get
            {
                return userId;
            }
        }
        protected int limit;

        public User (string pickName, string id, int limit)
        {
            this.pickName = pickName;
            this.userId = id;
            this.limit = limit;
        }
    }
}


