﻿<%@ Page Title="" Language="C#" MasterPageFile="~/PersonalCenterMenu.master" AutoEventWireup="true" CodeBehind="ResetPassWordPage.aspx.cs" Inherits="BBS.ResetPassWordPage" %>
<asp:Content ID="Content1" ContentPlaceHolderID="PersonalContent" runat="server">
    <table style="width:50%; margin:0px auto;">
        <tr>
            <td>旧密码:</td>
            <td>
                <asp:TextBox ID="txtUsingPwd" runat="server"></asp:TextBox>
                <asp:RequiredFieldValidator ControlToValidate="txtUsingPwd" runat="server" ErrorMessage="*" ForeColor="Red"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td>新密码:</td>
            <td>
                <asp:TextBox ID="txtNewPwd" runat="server"></asp:TextBox>
                <asp:RequiredFieldValidator ControlToValidate="txtNewPwd" runat="server" ErrorMessage="*" ForeColor="Red"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td>确认新密码:</td>
            <td>
                <asp:TextBox ID="txtEnsureNewPwd" runat="server"></asp:TextBox>
                <asp:RequiredFieldValidator ControlToValidate="txtEnsureNewPwd" runat="server" ErrorMessage="*" ForeColor="Red"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <asp:Button ID="btnSubmit" runat="server" Text="提交" OnClick="btnSubmit_Click" Width="70px" />
                <asp:CompareValidator runat="server" ControlToValidate="txtNewPwd" ControlToCompare="txtEnsureNewPwd" ErrorMessage="两次密码输入不一致" ForeColor="Red" Type="String"></asp:CompareValidator>
            </td>
        </tr>
    </table>
</asp:Content>
