﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SQLHelper;

namespace BBS
{
    public partial class PublishThemePage : BasePage
    {
        protected void Page_Load (object sender, EventArgs e)
        {

        }

        protected void btnPublish_Click (object sender, EventArgs e)
        {
            string title = this.txtTitle.Text;
            string content = this.txtContent.Text.Replace("\r\n", "<br />");
            string publishDate = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
            User user = (User)Session["user"];
            int userId = int.Parse(user.UserId);

            string createThemeStr = string.Format("INSERT INTO THEME(TITLE,CONTENT,ACCOUNT_ID,PUBLISH_DATE) VALUES('{0}','{1}',{2},'{3}');", title, content, userId, publishDate);
            SQLHelperSingleton.TryExecuteNonQuery(createThemeStr, this.Fail);

            this.Alert("发布成功");
            this.txtContent.Text = "";
            this.txtTitle.Text = "";
        }

        private void Fail (string err)
        {
            this.Alert(err);
        }
    }
}