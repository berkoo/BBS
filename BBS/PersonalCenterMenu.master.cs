﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace BBS
{
    public partial class PersonalCenterMenu : System.Web.UI.MasterPage
    {
        protected void Page_Load (object sender, EventArgs e)
        {

        }

        protected void SignOut_OnClick (object sender, EventArgs e)
        {
            Session["user"] = null;
            Response.Redirect("Index.aspx");
        }
    }
}