﻿<%@ Page Title="" Language="C#" MasterPageFile="~/UserServerMasterPage.master" AutoEventWireup="true" CodeBehind="Index.aspx.cs" Inherits="BBS.Index" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPage" runat="server">
    <asp:GridView ID="gvThemes" runat="server" Width="100%" AllowPaging="True" HorizontalAlign="Center" GridLines="None" OnPageIndexChanging="gvThemes_PageIndexChanging" AutoGenerateColumns="False" HeaderStyle-HorizontalAlign="Left" CellPadding="4" ForeColor="#333333">
        <AlternatingRowStyle BackColor="White" />
        <Columns>
            <asp:TemplateField HeaderText="标题" ItemStyle-Width="60%">
                <ItemTemplate>
                    <asp:HyperLink ID="hlTitle" runat="server" NavigateUrl='<%#"ThemeDetailPage.aspx?themeId="+Eval("THEME_ID") %>' Text='<%#Eval("TITLE") %>'>'>
                    </asp:HyperLink>
                </ItemTemplate>

<ItemStyle Width="60%"></ItemStyle>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="发布者" ItemStyle-Width="20%">
                <ItemTemplate>
                    <asp:Label runat="server" Text='<%#Eval("PICKNAME") %>'></asp:Label>
                </ItemTemplate>

<ItemStyle Width="20%"></ItemStyle>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="发布日期" ItemStyle-Width="20%">
                <ItemTemplate>
                    <asp:Label runat="server" Text='<%#Eval("PUBLISH_DATE") %>'>'></asp:Label>
                </ItemTemplate>

<ItemStyle Width="20%"></ItemStyle>
            </asp:TemplateField>
        </Columns>
        <EditRowStyle BackColor="#2461BF" />
        <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />

<HeaderStyle HorizontalAlign="Left" BackColor="#507CD1" Font-Bold="True" ForeColor="White"></HeaderStyle>

        <PagerSettings Position="TopAndBottom" NextPageText="下一页" PreviousPageText="上一页" />

        <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
        <RowStyle BackColor="#EFF3FB" />
        <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
        <SortedAscendingCellStyle BackColor="#F5F7FB" />
        <SortedAscendingHeaderStyle BackColor="#6D95E1" />
        <SortedDescendingCellStyle BackColor="#E9EBEF" />
        <SortedDescendingHeaderStyle BackColor="#4870BE" />

    </asp:GridView>
</asp:Content>
