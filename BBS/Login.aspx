﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="BBS.Login" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style>
        .row{
            width:30%;
            margin:0px auto;
        }
        .space{
            height:50px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Content1" runat="server">
    <div class="space"></div>
    <br />
    <div style="margin:0px auto; width:100%;">
        <div class="row">
            账号：<asp:TextBox ID="txtPickName" runat="server"></asp:TextBox>
            <asp:RequiredFieldValidator ID="rfvAccountId" runat="server" ErrorMessage="*" ForeColor="Red" ControlToValidate="txtPickName" ValidationGroup="login"></asp:RequiredFieldValidator>
        </div>
        <br />
        <div class="row">
            密码：<asp:TextBox ID="txtPwd" runat="server" TextMode="Password"></asp:TextBox>
            <asp:RequiredFieldValidator ID="rfvPwd" runat="server" ErrorMessage="*" ForeColor="Red" ControlToValidate="txtPwd" ValidationGroup="login"></asp:RequiredFieldValidator>
        </div>
        <br />
        <div class="row">
             <asp:Button ID="btnLogin" runat="server" Text="登陆" OnClick="btnLogin_Click" Width="59px" ValidationGroup="login" /> 
            <a href="RegisterPage.aspx" style="margin-left:10px;">注册</a>  
        </div>
    </div>
    <div class="space"></div>
    <br />
</asp:Content>
