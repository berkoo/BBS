﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace BBS
{
    public partial class ReplyView : System.Web.UI.UserControl
    {
        private ReplyModel model;
        public ReplyModel replyModel
        {
            set
            {
                this.model = value;
            }
            get
            {
                return this.model;
            }
        }

        //评论是第几楼
        public int Floor { set; get; }

        public delegate void ReplyDelegate(ReplyView replyView);
        public ReplyDelegate ReplyDel { get; set; }

        //要显示的回复内容
        protected string displayContent;

        protected void Page_Load (object sender, EventArgs e)
        {
            if ( !IsPostBack )
            {
                if ( model == null )
                {
                    return;
                }

                this.lblReplyAccount.Text = string.Format("{0}楼 @{1} 回复于 {2}", this.Floor, model.ReplyAccountPickName, model.ReplyDate);
            }
            if ( model.TargetReplyId > 0 )
            {
                this.displayContent = string.Format("回复@{0}<br />{1}", model.TargetAccountPickName, model.ReplyContent);
            } else
            {
                this.displayContent = model.ReplyContent;
            }
        }

        protected void btnReply_Click (object sender, EventArgs e)
        {
            if ( this.ReplyDel != null )
            {
                this.ReplyDel(this);
            }
        }
    }
}