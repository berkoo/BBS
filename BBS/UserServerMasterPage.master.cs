﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace BBS
{
    public partial class UserServerMasterPage : System.Web.UI.MasterPage
    {
        protected string pickName;
        protected void Page_Load (object sender, EventArgs e)
        {
            User user = (User)Session["user"];
            if(user != null)
                pickName = user.PickName;
        }

        protected void GoToPersonInfo_Click (object sender, EventArgs e)
        {
            Response.Redirect("PersonalCenter.aspx");
        }

        protected void GoToLogin_Click (object sender, EventArgs e)
        {
            Response.Redirect("Login.aspx");
        }

        protected void btnSearch_Click (object sender, EventArgs e)
        {
            string searchField = this.txtSearchField.Text;
            Response.Redirect(string.Format("Index.aspx?searchField={0}", searchField));
        }

        protected void GoToIndex_Click (object sender, EventArgs e)
        {
            Response.Redirect("Index.aspx");
        }
    }
}