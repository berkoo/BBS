﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.SessionState;
using SQLHelper;
using System.Configuration;

namespace BBS
{
    public class Global : System.Web.HttpApplication
    {

        protected void Application_Start (object sender, EventArgs e)
        {
            Application.Lock();
            //先配置好与数据库的连接
            string connStr = ConfigurationManager.AppSettings["ConnectionString"].ToString();
            SQLHelperSingleton.SetupDataBase(connStr);
            Application.UnLock();
        }

        protected void Session_Start (object sender, EventArgs e)
        {
            string id = Response.Cookies["user"]["id"];
            if ( !string.IsNullOrEmpty(id) )
            {
                string pickName = Response.Cookies["user"]["pickName"];
                int limit = int.Parse(Response.Cookies["user"]["limit"]);
                User user = new User(pickName, id, limit);
                this.Session["user"] = user;

                //每次访问并且没有重新登录过，则让cookie的过期时间延迟一周
                Response.Cookies["user"].Expires = DateTime.Now.AddDays(7);
            }
        }

        protected void Application_BeginRequest (object sender, EventArgs e)
        {

        }

        protected void Application_AuthenticateRequest (object sender, EventArgs e)
        {

        }

        protected void Application_Error (object sender, EventArgs e)
        {

        }

        protected void Session_End (object sender, EventArgs e)
        {

        }

        protected void Application_End (object sender, EventArgs e)
        {

        }
    }
}