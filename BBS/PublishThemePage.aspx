﻿<%@ Page Title="" Language="C#" MasterPageFile="~/PersonalCenterMenu.master" AutoEventWireup="true" CodeBehind="PublishThemePage.aspx.cs" Inherits="BBS.PublishThemePage" %>
<asp:Content ID="Content1" ContentPlaceHolderID="PersonalContent" runat="server">
    <div style="margin:0px auto; width:80%;">
        <table style="width:100%;">
            <tr>
                <td>标题:</td>
                <td>
                    <asp:TextBox ID="txtTitle" runat="server" Width="100%"></asp:TextBox>
                    <asp:RequiredFieldValidator runat="server" ControlToValidate="txtTitle" ErrorMessage="*" ForeColor="Red"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>正文:</td>
                <td>
                    <asp:TextBox ID="txtContent" runat="server" TextMode="MultiLine" Width="100%" Height="200px"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <asp:Button ID="btnPublish" runat="server" Text="发布" OnClick="btnPublish_Click" />
                </td>
            </tr>
        </table>
    </div>
</asp:Content>
