﻿<%@ Page Title="" Language="C#" MasterPageFile="~/UserServerMasterPage.master" AutoEventWireup="true" CodeBehind="ThemeDetailPage.aspx.cs" Inherits="BBS.ThemeDetailPage" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPage" runat="server">
    <style>
        .right {
            float:right;
        }
        .left{
            float:left;
        }
        .textLeft{
            text-align:left;
        }
    </style>
    <table style="width:100%; border-width:0px; text-align:center;">
        <tr>
            <td><asp:Label ID="lblTitle" runat="server" Text=""></asp:Label></td>
        </tr>
        <tr>
            <td>楼主:&nbsp;<asp:Label ID="lblAccount" runat="server"></asp:Label>&nbsp;
            发表于&nbsp;<asp:Label ID="lblPublishDate" runat="server"></asp:Label></td>
        </tr>
        <tr style="text-align:left; background-color:lightgray;">
            <td><%=themeContent %></td>
        </tr>
        <tr style="background-color:lightgray;">
            <td><asp:LinkButton ID="lbReply" runat="server" Text="回复" CssClass="right" OnClick="lbReplyMaster_Click"></asp:LinkButton></td>
        </tr>
        <tr>
            <td><asp:Panel ID="panReply" runat="server" Width="100%"></asp:Panel></td>
        </tr>
        <tr>
            <td style="height:10px;"></td>
        </tr>
        <tr>
            <td>
                <asp:Label runat="server" ID="lblReplyInfo" Text="" Width="100%" CssClass="textLeft" Height="20px"></asp:Label><br />
                <asp:TextBox ID="txtReplyContent" runat="server" Width="80%" Height="200px" CssClass="left" TextMode="MultiLine" ></asp:TextBox>

            </td>
        </tr>
        <tr>
            <td><asp:Button ID="btnReply" runat="server" Text="发表" CssClass="left" OnClick="btnReply_Click" /></td>
        </tr>
    </table>
</asp:Content>
