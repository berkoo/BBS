﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ReplyView.ascx.cs" Inherits="BBS.ReplyView" %>
<asp:Panel runat="server" ID="panel1">
    <style type="text/css">
        .contentBg{
            background-color:lightgray;
        }
    </style>
    <table style="width:100%; border-width:0px; border-color:black; text-align:left;">
        <tr>
            <td><asp:Label runat="server" ID="lblReplyAccount"></asp:Label></td>
        </tr>
        <tr class="contentBg">
            <td><%=displayContent %></td>
        </tr>
        <tr class="contentBg">
            <td>
                <div style="float:right;">
                    <asp:LinkButton runat="server" ID="btnReply" OnClick="btnReply_Click" Text="回复"></asp:LinkButton>
                </div>
            </td>
        </tr>
    </table>
</asp:Panel>